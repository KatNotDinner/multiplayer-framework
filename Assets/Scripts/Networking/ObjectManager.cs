﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.Networking;

[AddComponentMenu("Scripts/World/Object Manager")]
public class ObjectManager : MonoBehaviour {
    public uint maxObjects = 2000000;
    public GameObject[] Prefabs;

    Dictionary<uint, GameObject> objects = new Dictionary<uint, GameObject>();
    Dictionary<GameObject, uint> ids = new Dictionary<GameObject, uint>();
    NetworkController net;
    ServerController server;
    ClientController client;
    GameObject playerPrefab;
    uint spawnedObjects = 0;

    void Start() {
        net = GetComponent<NetworkController>();
        server = GetComponent<ServerController>();
        client = GetComponent<ClientController>();

        playerPrefab = Prefabs[0];

        net.AddMessageHook(NetworkMessage.SpawnObject, ObjectSpawned);
    }

    [Serializable]
    public class ObjectData {
        public uint netId;
        public uint prefabId;
        public DataConverter.SVector3 pos;
        public DataConverter.SQuaternion ang;
    }

    uint AssignID(GameObject obj) {
        if (ids.ContainsKey(obj)) {
            return ids[obj];
        }

        uint curId = 0;
        for (uint i = 0; i < maxObjects; i++) {
            if (objects.ContainsKey(i)) {
                continue;
            }

            curId = i;
            break;
        }

        objects.Add(curId, obj);
        ids.Add(obj, curId);
        uint temp = curId;
        return temp;
    }

    public void SetComponentValue<T>(GameObject obj, FieldInfo field, object value) {
        // TODO: Use Field.SetValue(obj, val) to set values for components accross the network
        field.SetValue(obj.GetComponent<T>(), value);
        // TODO: Network and test
    }

    public GameObject GetPrefab(uint id) {
        if (Prefabs[id] == null) {
            throw new IndexOutOfRangeException();
        }

        return Prefabs[id];
    }

    public GameObject SpawnObject(GameObject prefab) {
        return SpawnObject(prefab, Vector3.zero, Quaternion.identity);
    }

    public GameObject SpawnObject(GameObject prefab, GameObject spawnPoint) {
        if (spawnPoint == null) {
            Debug.LogError("Nonexistant spawnpoint");
            throw new System.NullReferenceException();
        }

        return SpawnObject(prefab, spawnPoint.transform.position, spawnPoint.transform.rotation);
    }

    public GameObject SpawnObject(ObjectData objData) {
        GameObject prefab;
        try {
            prefab = GetPrefab(objData.prefabId);
        }
        catch (IndexOutOfRangeException e) {
            Debug.LogError("Unexistant prefab " + objData.prefabId);
            throw e;
        }

        DataConverter conv = new DataConverter();

        return SpawnObject(prefab, conv.Convert(objData.pos), conv.Convert(objData.ang));
    }

    public GameObject SpawnObject(GameObject prefab, Vector3 spawnPoint, Quaternion spawnAngles) {
        if (prefab == null) {
            Debug.LogError("Nonexistant prefab");
            throw new System.NullReferenceException();
        }

        GameObject obj;
        obj = Instantiate(prefab, spawnPoint, spawnAngles);

        if (NetworkController.isServer) {
            // TODO: Update spawned objects
            bool found = false;
            uint i;
            for (i = 0; i < Prefabs.Length; i++) {
                if (prefab != Prefabs[i]) {
                    continue;
                }
                found = true;
            }

            if (!found) {
                return obj;
            }

            spawnedObjects++;

            DataConverter converter = new DataConverter();
            ObjectData data = new ObjectData();
            data.pos = converter.Convert(obj.transform.position);
            data.ang = converter.Convert(obj.transform.rotation);
            data.netId = AssignID(obj);
            Debug.Log("ID is " + data.netId);

            server.SendToAll(NetworkMessage.SpawnObject, data, net.reliableChannel);
        }

        return obj;
    }

    public bool IsObjectNetworked(GameObject obj) {
        return ids.ContainsKey(obj);
    }

    public uint GetObjectID(GameObject obj) {
        if (!IsObjectNetworked(obj)) {
            throw new ArgumentNullException();
        }
        return ids[obj];
    }

    public GameObject GetObjectByID(uint id) {
        if (!objects.ContainsKey(id)) {
            throw new ArgumentNullException();
        }
        return objects[id];
    }
    
    public GameObject SpawnPlayer(int conId) {
        GameObject player = SpawnObject(playerPrefab, GameObject.FindWithTag("Respawn"));
        if (NetworkController.isServer) {

        }
        return player;
    }

    void ObjectSpawned(int conId, object data) {
        // TODO: Move this to ObjectManager
        Debug.Log("Received object");
        ObjectData objData = (ObjectData)data;
        Debug.Log(objData.prefabId);
        Debug.Log("ID is " + objData.netId);
        
        SpawnObject(objData);
        //client.SendMessage(NetworkMessage.SpawnObject, NetworkTransport.GetNetworkTimestamp(), net.reliableChannel);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
