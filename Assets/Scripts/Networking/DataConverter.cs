﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[AddComponentMenu("Scripts/Utilities/Data Converter")]
public class DataConverter {
    [System.Serializable]
    public class SVector3 {
        public float x, y, z;
    }

    [System.Serializable]
    public class SQuaternion {
        public float x, y, z, w;
    }

    public SVector3 Convert(Vector3 vec) {
        return new SVector3 { x = vec.x, y = vec.y, z = vec.z };
    }
    
    public SQuaternion Convert(Quaternion qrt) {
        return new SQuaternion { x = qrt.x, y = qrt.y, z = qrt.z, w = qrt.w };
    }

    public Vector3 Convert(SVector3 vec) {
        return new Vector3 { x = vec.x, y = vec.y, z = vec.z };
    }

    public Quaternion Convert(SQuaternion qrt) {
        return new Quaternion { x = qrt.x, y = qrt.y, z = qrt.z, w = qrt.w };
    }
}
