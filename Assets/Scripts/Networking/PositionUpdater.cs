﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[AddComponentMenu("Scripts/Networking/Position Updater")]
public class PositionUpdater : MonoBehaviour {
    [Serializable]
    struct PositionData {
        public uint netId;
        public DataConverter.SVector3 pos;
        public DataConverter.SQuaternion ang;
        public int timeStamp;
    }

    public bool UpdatePosition = true;
    ClientController client;
    ServerController server;
    NetworkController net;
    ObjectManager objManager;
    Action ControllerUpdate;
    int timeStamp;

    // Use this for initialization
    void Start() {
        objManager = FindObjectOfType<ObjectManager>();
        net = FindObjectOfType<NetworkController>();

        if (NetworkController.isServer) {
            server = FindObjectOfType<ServerController>();
            Debug.Log("Assigned server controller " + server);
            ControllerUpdate = ServerUpdate;
            net.AddMessageHook(NetworkMessage.SpawnObject, SpawnObjectRequest);
        }
        else {
            client = FindObjectOfType<ClientController>();
            Debug.Log("Assigned client controller " + client);
            ControllerUpdate = ClientUpdate;
            net.AddMessageHook(NetworkMessage.SyncPosition, OnPositionUpdate);
        }
    }

    void SpawnObjectRequest(int conId, object dataReceived) {
        // TODO: Move to ServerUpdate()
        /*PositionData data = new PositionData();
        DataConverter conv = new DataConverter();
        data.pos = conv.Convert(transform.position);
        data.ang = conv.Convert(transform.rotation);
        data.netId = objectManager.GetObjectID(gameObject);
        data.timeStamp = NetworkTransport.GetNetworkTimestamp();
        Debug.Log(data.timeStamp);*/
    }

    void PrintDelay() {
        // TODO: Delete this after testing
        byte error;
        int delay = client.GetRemoteDelay(timeStamp, out error);
        if (error != 0) {
            Debug.LogError((NetworkError)error);
        }

        int rtt = client.GetCurrentRTT(out error);
        if (error != 0) {
            Debug.LogError((NetworkError)error);
        }

        Debug.Log(delay + " " + (((float) rtt) * 0.001f));
    }

    void ChangeTimestamp() {
        timeStamp = NetworkTransport.GetNetworkTimestamp();
    }

    void OnPositionUpdate(int conId, object dataReceived) {
        // TODO Change this to the proper implementation after testing
        Debug.Log("Position update");
        //PositionData data = (PositionData)dataReceived;
        timeStamp = (int)dataReceived;
        Debug.Log(timeStamp + " " + net.GetNetworkTimestamp());
        PrintDelay();
        //Invoke("ChangeTimestamp", 1.9f);
        Invoke("PrintDelay", 60);
    }

    void ClientUpdate() {

    }
	
    void ServerUpdate() {

    }

	// Update is called once per frame
	void Update () {
        ControllerUpdate();
	}
}
