﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkErrorException : Exception {
    public NetworkError error { get; private set; }

    public NetworkErrorException(NetworkError error) {
        this.error = error;
    }
}
