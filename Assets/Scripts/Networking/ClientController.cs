﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

[AddComponentMenu("Scripts/Networking/Client Controller")]
public class ClientController : MonoBehaviour, INetworkedInstance {
    NetworkController net;
    ObjectManager objManager;

    public string IP = "127.0.0.1";
    public int port = 7777;

    // TODO: Possibly encapsulate these
    public int hostId { get; private set; } // Socket
    public int conId { get; private set; } // Connection ID
    public double time { get; private set; } // TODO: Implement this
    
    bool active = false;
    
    public void SetupClient() {
        net = GetComponent<NetworkController>();
        objManager = GetComponent<ObjectManager>();

        hostId = net.Init(0);
        Connect();

        GetComponent<ServerController>().enabled = false;
        
        net.AddMessageHook(NetworkMessage.LoadScene, OnLoadSceneMessage);
    }

    void Connect() {
        byte error;
        conId = NetworkTransport.Connect(hostId, IP, port, 0, out error);

        if ((NetworkError)error != NetworkError.Ok) {
            Debug.LogError((NetworkError)error);
        }

        net.AddMessageHook(NetworkMessage.SyncTime, OnSyncTimeMessage);

        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSyncTimeMessage(int conId, object dataReceived) {

    }

    public void Disconnect() {
        byte error;
        NetworkTransport.Disconnect(hostId, conId, out error);
        
        if((NetworkError) error != NetworkError.Ok) {
            Debug.Log((NetworkError)error);
        }
    }

    // Use this for initialization
    void Start () {
        net = GetComponent<NetworkController>();
    }

    void OnLoadSceneMessage(int conId, object data) {
        // Load scene
        int id = (int)data;
        Debug.Log("Loading scene " + id);
        if (SceneManager.GetActiveScene().buildIndex == id) {
            return;
        }

        SceneManager.LoadScene(id);
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        net.SendMessage(hostId, conId, NetworkMessage.SceneLoaded, net.reliableChannel);
    }

    public NetworkError SendMessage(NetworkMessage id, object data, int channel) {
        return net.SendMessage(hostId, conId, id, data, channel);
    }

    public void OnConnect(int outHostId, int outConId, NetworkError error) {
        if (error != NetworkError.Ok) {
            Debug.LogError(error);
        }

        Debug.Log("Connected");
    }

    public void OnData(int outHostId, int outConId, int outChanId, byte[] buffer, int size, NetworkError error) {

    }

    public void OnDisconnect(int outHostId, int outConId, NetworkError error) {
        if (error != NetworkError.Ok) {
            Debug.Log(error);
        }
        Debug.Log("Disconnected");
    }

    public void OnBroadcast(int outHostId, byte[] buffer, int size, NetworkError error) {
        if (error != NetworkError.Ok) {
            Debug.Log(error);
        }
        Debug.Log("Data received via broadcast");
    }
    
    public int GetRemoteDelay(out byte error) {
        return GetRemoteDelay(net.GetNetworkTimestamp(), out error);
    }

    public int GetRemoteDelay(int timeStamp, out byte error) {
        int time = NetworkTransport.GetRemoteDelayTimeMS(hostId, conId, timeStamp, out error);
        return time;
    }

    public int GetCurrentRTT(out byte error) {
        // TODO: Move this to NetworkController after testing
        return NetworkTransport.GetCurrentRTT(hostId, conId, out error);
    }

    // Update is called once per frame
    void Update() {

    }
}
