﻿using System.Collections;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

[AddComponentMenu("Scripts/Networking/Server Controller")]
public class ServerController : MonoBehaviour, INetworkedInstance {
    NetworkController net;
    ObjectManager objectManager;

    public string IP = "127.0.0.1";
    public int port = 7777;
    public int maxPlayers = 32;
    public double time { get; private set; } // TODO: Implement this

    public GameObject testObject;

    // TODO: Possibly encapsulate these
    int hostId;
    int conId;

    struct playerData {
        public int hostId;
        public int conId;
        public bool active;
    }

    int index = 0;
    playerData[] players;

    public void SetupServer() {
        // TODO: Play around with settings
        hostId = net.Init(port);
        
        GetComponent<ClientController>().enabled = false;
        
        net.AddMessageHook(NetworkMessage.SceneLoaded, OnPlayerLoadedScene);
    }

    void OnPlayerLoadedScene(int conId, object dataReceived) {
        Debug.Log("Scene loaded from connection " + conId);

        // THIS IS JUST FOR TESTING
        objectManager.SpawnObject(testObject); // Remove after testing

        SendToAll(NetworkMessage.SyncPosition, NetworkTransport.GetNetworkTimestamp(), net.reliableChannel); // Remove after testing
    }

    void LoadScene(int id) {
        Scene curScene = SceneManager.GetActiveScene();
        Debug.Log("Active scene " + curScene.buildIndex);

        if (id != curScene.buildIndex) {
            //SceneManager.UnloadSceneAsync(curScene.buildIndex);
            SceneManager.LoadScene(id);
        }

        SendToAll( NetworkMessage.LoadScene, id, net.reliableChannel );
    }

	// Use this for initialization
	void Start () {
        net = GetComponent<NetworkController>();
        objectManager = GetComponent<ObjectManager>();

        players = new playerData[maxPlayers];

        for (int i = 0; i<maxPlayers; i++) {
            players[i].active = false;
        }
    }

    public NetworkError SendMessage(int playerIndex, NetworkMessage id, object data, int channel) {
        return net.SendMessage(players[playerIndex].hostId, players[playerIndex].conId,  id, data, channel);
    }

    public void SendToAll(NetworkMessage id, object data, int channel) {
        for (int i = 0; i<maxPlayers; i++) {
            if (!players[i].active) {
                continue;
            }

            NetworkError err = net.SendMessage(players[i].hostId, players[i].conId, id, data, channel);

            if (err != NetworkError.Ok) {
                Debug.LogError(err);
            }
        }
    }

    public void OnConnect(int outHostId, int outConId, NetworkError error) {
        Debug.Log("Player connected (" + outConId + ")");
        Debug.Log(outHostId == hostId);
        // TODO: Change playerIds to smply connection ids
        players[index].hostId = outHostId;
        players[index].conId = outConId;
        players[index].active = true;
        index++;

        if (index >= maxPlayers) {
            index = 0;
        }

        LoadScene(1);
    }

    public void OnData(int outHostId, int outConId, int outChanId, byte[] buffer, int size, NetworkError error) {

    }

    public void OnDisconnect(int outHostId, int outConId, NetworkError error) {
        Debug.Log("Player disconnected (" + outConId + ")");
        for (int i = 0; i<maxPlayers; i++) {
            if (players[i].conId != outConId) {
                continue;
            }

            players[i].active = false;
        }
    }

    public void OnBroadcast(int outHostId, byte[] buffer, int size, NetworkError error) {
        Debug.Log("Data received via broadcast");
    }

    // Update is called once per frame
    void Update() {

    }
}
