﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public interface INetworkedInstance {
    void OnConnect(int outHostId, int outConId, NetworkError error);
    void OnDisconnect(int outHostId, int outConId, NetworkError error);
    void OnData(int outHostId, int outConId, int outChanId, byte[] buffer, int size, NetworkError error);
    void OnBroadcast(int outHostId, byte[] buffer, int size, NetworkError error);
}
