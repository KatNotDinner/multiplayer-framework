﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.Networking;

public enum NetworkMessage : ushort {
    LoadScene = 0,
    SceneLoaded = 1,
    SpawnObject = 2,
    AssignPlayer = 3,
    SyncPosition = 4,
    SyncTime = 5
    };

// <summary>
// A component that manages the shared networked code
// </summary>
[AddComponentMenu("Scripts/Networking/Network Controller")]
public class NetworkController : MonoBehaviour {
    INetworkedInstance ctrl;

    public int reliableChannel { get; private set; }
    public int reliableSequencedChannel { get; private set; }
    public int stateUpdateChannel { get; private set; }
    public int unreliableChannel { get; private set; }

    public static bool isServer { get; private set; }
    bool isActive = false;

    Hashtable hooks = new Hashtable();

    public int Init(int port) {
        GlobalConfig gCfg = new GlobalConfig {
            MaxPacketSize = 1510
        };
        NetworkTransport.Init(gCfg);

        ConnectionConfig conCfg = new ConnectionConfig();
        reliableChannel = conCfg.AddChannel(QosType.Reliable);
        reliableSequencedChannel = conCfg.AddChannel(QosType.ReliableSequenced);
        stateUpdateChannel = conCfg.AddChannel(QosType.ReliableStateUpdate);
        unreliableChannel = conCfg.AddChannel(QosType.Unreliable);

        HostTopology tpg = new HostTopology(conCfg, 10);

        int hostId = NetworkTransport.AddHost(tpg, port);

        isActive = true;

        if (port > 0) {
            isServer = true;
            ctrl = GetComponent<ServerController>();
            return hostId;
        }
        
        isServer = false;
        ctrl = GetComponent<ClientController>();
        return hostId;
    }
    
    void Start() {
        DontDestroyOnLoad(gameObject);

        isServer = false;
    }

    public NetworkError SendMessage(int outHostId, int outConId, NetworkMessage id, int channel) {
        // Binding for a message without data
        return SendMessage(outHostId, outConId, (ushort)id, null, channel);
    }

    public NetworkError SendMessage(int outHostId, int outConId, NetworkMessage id, object message, int channel) {
        return SendMessage(outHostId, outConId, (ushort)id, message, channel);
    }

    NetworkError SendMessage(int outHostId, int outConId, ushort id, object message, int channel) {
        // Internal implementation that uses ushort for IDs
        byte[] buffer = new byte[1024];
        byte err;
        Stream msg = new MemoryStream(buffer);
        BinaryFormatter formatter = new BinaryFormatter();
        formatter.Serialize(msg, id);
        // Serialize message id and data (if there's any)
        if (message != null) {
            formatter.Serialize(msg, message);
        }

        NetworkTransport.Send(outHostId, outConId, channel, buffer, (int) msg.Position, out err);

        return (NetworkError)err;
    }

    bool RunHook(ushort id, int conId, object obj) {
        // Runs the respective hooks and returns false if they don't exist
        if (!hooks.ContainsKey(id)) {
            return false;
        }

        foreach (Action<int, object> func in (List<Action<int, object>>) hooks[id]) {
            func(conId, obj);
        }
        return true;
    }

    public void AddMessageHook(NetworkMessage id, Action<int, object> func) {
        if (!hooks.ContainsKey((ushort)id)) {
            hooks.Add((ushort)id, new List<Action<int, object>>());
        }
        ((List<Action<int, object>>) hooks[(ushort)id]).Add(func);
    }

    public void RemoveMessageHook(ushort id) {
        if (!hooks.ContainsKey(id)) {
            throw new IndexOutOfRangeException();
        }
        
        hooks.Remove(id);
    }

    public void RemoveMessageHook(ushort id, Action<int, object> func) {
        if (!hooks.ContainsKey(id)) {
            throw new IndexOutOfRangeException();
        }

        ((List<Action<int, object>>)hooks[id]).Remove(func);
    }

    public int GetNetworkTimestamp() {
        return NetworkTransport.GetNetworkTimestamp();
    }

    void Listen() {
        int outHostId;
        int outConId;
        int outChanId;
        int receivedSize;
        // TODO: Fit packet size and buffer size
        byte[] buffer = new byte[1024];
        byte error;
        NetworkEventType netEvent = NetworkTransport.Receive(out outHostId, out outConId, out outChanId, buffer, buffer.Length, out receivedSize, out error);

        if ((NetworkError)error != NetworkError.Ok) {
            Debug.Log((NetworkError)error);
        }

        switch (netEvent) {
            // Forward NetworkTransport bindings to the respective controller
            case NetworkEventType.ConnectEvent: {
                    ctrl.OnConnect(outHostId, outConId, (NetworkError)error);
                    break;
                }

            case NetworkEventType.DisconnectEvent: {
                    ctrl.OnDisconnect(outHostId, outConId, (NetworkError)error);
                    break;
                }

            case NetworkEventType.DataEvent: {
                    OnData(outHostId, outConId, outChanId, buffer, receivedSize, (NetworkError)error);
                    ctrl.OnData(outHostId, outConId, outChanId, buffer, receivedSize, (NetworkError)error);
                    break;
                }

            case NetworkEventType.BroadcastEvent: {
                    ctrl.OnBroadcast(outHostId, buffer, receivedSize, (NetworkError)error);
                    break;
                }

            case NetworkEventType.Nothing: {
                    break;
                }

            default: {
                    Debug.Log("Unknown message received");
                    break;
                }
        }
    }

    void OnData(int outHostId, int outConId, int outChanId, byte[] buffer, int size, NetworkError error) {
        // Deserialize the data and pass it to hooks
        Stream msg = new MemoryStream(buffer);
        BinaryFormatter format = new BinaryFormatter();
        ushort id = (ushort)format.Deserialize(msg);
        object obj = null;
        if (msg.Position < size) {
            obj = format.Deserialize(msg);
        }
        
        if (!RunHook(id, outConId, obj)) {
            Debug.LogError("Tried to run a hook for unregistered message " + id);
        }
    }
    // Update is called once per frame
    void Update() {
        if (!isActive) {
            return;
        }

        Listen();
    }
}
