﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Scripts/UI/Test/Disconnect Button")]
public class BDisconnect : MonoBehaviour {
    public GameObject netManager;

    public void OnClick() {
        netManager.GetComponent<ClientController>().Disconnect();
    }
    
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
