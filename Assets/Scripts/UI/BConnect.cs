﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Scripts/UI/Test/Client Button")]
public class BConnect : MonoBehaviour {
    public GameObject netManager;

    public void OnClick() {
        Debug.Log("Client");
        //netManager.GetComponent<NetworkManagment>().SetupClient();
        netManager.GetComponent<ClientController>().SetupClient();
    }

   	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
