﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Scripts/UI/Test/Server Button")]
public class BStartServer : MonoBehaviour {
    public GameObject netManager;

    public void OnClick() {
        Debug.Log("Server");
        //netManager.GetComponent<NetworkManagment>().SetupServer();
        netManager.GetComponent<ServerController>().SetupServer();
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
