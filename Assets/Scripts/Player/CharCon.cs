﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Scripts/Player/Character Controller")]
public class CharCon : MonoBehaviour {

    public float speedScale = 1.0f;
    public float rotationScale = 1.0f;

    static readonly uint forward = 1;
    static readonly uint back = 1 << 1;
    static readonly uint left = 1 << 2;
    static readonly uint right = 1 << 3;

    public Vector3 velocity;

    public struct inputStruct {
        public float x;
        public float y;
        public float v;
        public float h;
        public float deltaTime;
    }

    private int index;
    public int max;
    public inputStruct[] inputs;

    // FOR TESTING
    public bool simulate = false;
    public bool recvInput = false;
    public float trigger;
    private Vector3 spawnPos;
    // FOR TESTING

    private CharacterController ctrl;

    // Use this for initialization
    void Start () {
        index = 0;
        inputs = new inputStruct[max];
        ctrl = GetComponent<CharacterController>();
        spawnPos = transform.position;
        trigger = -1;
    }

    void Simulate( float dt ) {
        // TODO: Use Move() but use velocity * dt to simulate the length of the frame
        // IMPORTANT: Use velocity += Physics.gravity and some multiplier
        // Also use CalculateMovement/Rotations with the appropriate 

        // Networked code should look something like this
        Vector3 rot = CalculateRotation(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y")) * dt;
        Rotate(rot);

        Vector3 mov = CalculateMovement(Input.GetAxis("Vertical"), Input.GetAxis("Horizontal")) * dt;

        ctrl.Move(mov);
    }

    void Simulate( inputStruct input ) {
        // TODO: Use Move() but use velocity * dt to simulate the length of the frame
        // IMPORTANT: Use velocity += Physics.gravity and some multiplier
        // Also use CalculateMovement/Rotations with the appropriate 

        // Networked code should look something like this
        Vector3 rot = CalculateRotation(input.x, input.y) * input.deltaTime;
        Rotate(rot);

        Vector3 mov = CalculateMovement(input.v, input.h) * input.deltaTime;

        ctrl.Move(mov);
    }

    Vector3 CalculateRotation(float x, float y) {
        Vector3 temp = Vector3.zero;

        temp = Vector3.zero;
        
        temp.x = y * rotationScale;
        temp.y = x * rotationScale;
        return temp;
    }

    Vector3 CalculateMovement(float v, float h) {
        Vector3 temp = Vector3.zero;
        temp.x = h * 0.01f * speedScale;
        temp.z = v * 0.01f * -speedScale;

        temp = Quaternion.Euler(0.0f, transform.rotation.eulerAngles.y, 0.0f) * temp;
        return temp;
    }

    void Rotate(Vector3 rot) {
        transform.Rotate(Vector3.up, rot.y);
        transform.Rotate(Vector3.right, rot.x);
        transform.Rotate(Vector3.forward, -transform.eulerAngles.z);
    }

    void saveInput() {
        // Just for testing
        if ((trigger > 0) && (Time.time >= trigger)) {
            trigger = 0;
            recvInput = true;
            transform.position = spawnPos;
        }

        if (index < 0) {
            return;
        }

        inputs[index].v = Input.GetAxis("Vertical");
        inputs[index].h = Input.GetAxis("Horizontal");
        inputs[index].deltaTime = Time.deltaTime;

        index++;

        if (index >= max) {
            simulate = true;
            index = -1;
        }
    }

    // Horizontal axis = a/d
    // Vertical axis = w/s
    void FixedUpdate() {
      
    }

    // Update is called once per frame
    void Update () {
        // TODO: Use this in a packet
        // uint input = 0;

        // For now use a dumb structure to make testing easier

        // IMPORTANT:   Upon receiving input, store the timestamp of the last known input
        //              and discard all older that are received later respectively

        saveInput();

        if (ctrl == null) {
            return;
        }

        if (simulate) {
            // Simulate
            index = 0;
            while (index < max) {
                Simulate(inputs[index]);
                index++;
            }

            index = -1;

            simulate = false;
            trigger = Time.time + 1.0f;
        }

        if (!recvInput) {
            return;
        }

        Simulate(Time.deltaTime);
        //Physics.Simulate(Time.fixedDeltaTime);
    }
}
